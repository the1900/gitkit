''''
# -*- coding: utf-8 -*-



이런식으로 주석 여러줄로 만드는게 가능하다
이렇모양 그대로 나온당 ------ ㅇ
또 한줄 주석은 # 이런식으로 사용한다.



이런식으로 치환이 가능하다 
(a, b) = (1, 2)
[c, d] = [3, 4]



파이썬2 sys.maxint 하면 시스템이 허용하는 최대 int +1 하면 long으로 바뀜
파이썬3 sys.maxsize 하면 시스템 표현가능한 최대숫자
파이썬2 int / int는 int
파이썬3 int / int는 float 정수형 나누기는 //를 사용한


내부적으로 2에선 아스키와 유니코드를 분리했던거와 달리 3는 내부적으로 유니코드를 사용하며
utf-8 등의 인코딩을 지정하면 bytes가 되며 앞에 b가생긴다
 type("테스트".encode('cp949'))  인코딩이있는 문자열은 bytes가 된다
 ord() 를통해 해당하는 유니코드를 반환
 chr()을 통해 해당하는 유니코드 문자로 반환
 그냥 영어이외의 문자를 사용하면 유니코드이며
 sys.stdout.encoding 나 stdin 등을 이용해 시스템의 인코딩을 확인할수있다
 



전부 각 진수에맞는 문자열을 반환한다 숫자가 아니다
oct() 0o
hex() 0x
bin() 0b



int, float(지수형으로 자연로그 e 표현가능), complex

ㄴ\형변환 가능 
str()
int()
float()

List - 순서 존재함 인덱싱, 슬라이싱 가능 pop을통해 값반환과 동시에 제거가능
Set - 집합과 동일하며 union(합칩합) intersection(교집합)등의 연산자 지원 순서가 없다'
Dic - 사전(자바의 해쉬맵같음) 인덱싱x 추가할당은 a["키"]=값 형식으로 가능
       키, 아이템등 반환하는 함수가 2에서는 리스트였지만 3부턴 dict으로 반환된다

bool - True 와 False 가들어있는거

'''

def StrCal():
  print('py'*3)
  print('py'+'됨?')

  str = "kit2013"
  print( str[1]+str[0] ) #인덱싱
  #아래는 슬라이싱 
  print( str[:] )
  print( str[1:3])
  print( str[-2:])
  print (str[:3])

def SallowCopy():
  shallow = [2,3,4]
  shallow2 = shallow
  shallow[1] = 254
  print( "{0}왼쪽은 원본 오른쪽은 복사본 {1}) \n보다시피 동시참조라 값이 동일함 ".format(shallow, shallow2) )
  print( "{0} 아이디는 {1}랑 동일하다".format( id(shallow), id(shallow2) ) )
#깊은 복사할때 리스트는 shallow2 = shallow[:]로 값 복사해도되지만 보통 copy모듈의 deepcopy 함수 쓴다

'''
global() 내장함수 함수객체 조회
함수는 무조건 값 한개만 리턴하지만 여러개 쓴경우는 한개의 튜플로 리턴된다

global 키워드를 통해 전역영역의 변수를 지역영역에서 사용가능하다
이름검색 규칙은 Local Global Built-in 순서로 이름을 검색한다

dir() 함수를 통해 l해당 로컬 scope에 해당ㅇ하는 이름들을 볼수있 ___builtins__ 을 살펴보면 내장함수 종류들이 나온다.
impoert 된 모듈들도 살펴볼수있다.
사용자 정의객체는 __dir__을 오버로딩하여 dir함수에표시할 내용들을 정의할수있다.

함수의 파아미터명을 사용하여 인자를 넘길때 명시적으로 넘겨줄수가 있다.
기본값을 지정하는것도 된다. 
*args는 가변변수 리스트를 나타낸다
**args 는 정의도지 않은 변수들을 사전형식으로 받으수있다. 인자 전달할때 인자명=값 형식으로 전달하면
받을떄 사전으로 받는다.. URL을 쉽게 조립가능하다.

'''

#가변인자 리스트를 이용하여 합집합을 만드는 함수 
def union2(*arg):
  res = []
  for item in arg:
    for x in item:
      if not x in res:
        res.append(x)
  return res

union2("HAM", "kit", "HMS")

#가변인자 사전을 이용해 URL 조립
def url(port, server, **arg):
  target = "http://" + server + ":" + port+"?"
  for key in arg:
    target +=key+"="+arg[key]+"&"
  return target    

url(port="8080", server="wwwnaver.com", userid="kit2013", other="sometime")

'''
iter 객체를 통해 순회가 가능
next(ier객체)나 iter.__next__를통해 다음요소를 선택가
'''

'''
generator는 iterator를 만드는 도구이다
retturn대신 yield를 통해 스택의 메모리를 그대로 보관한다 원래는 return시 스택파괴됨
yield를 쓰면 해당 함수든 뭐든 코드가 제네레이터가 되는듯 
'''

'''
enumerate는 순회가능한 객체에서 인덱스와 같이 요소의 값을 반환한다
for i, season in enumerate(['spring', 'summer', 'fall', 'winter' ]):
2번째 인자로 시작할 인덱스값을 줄수있다 기본은 0이다
 print(i, season)
'''
def FibonacciGenerator():
  def Fibonacci():
    a, b = 1, 1
    while 1:
      yield a
      a, b = b, a + b
      
  for i, ret in enumerate(Fibonacci()):
    if i < 20: print(i, ret)
    else : break

def FibonachiRecursive(n):
  if n < 2: return n
  else: return Fibonachi_recursive(n-1) + Fibonachi_recursive(n-2)

'''
단축 평가( short-circuit evaluation)
if 거짓 & 참 일때 왼쪽이 이미 거짓이기 때문에 뒤는 참인지 판단할 필요가 없다. 이걸 단축평
은 &와 | 말고 and와 or 사용시 왼쪽 피연산자가 우변보다 먼저 단축 평가되도록 인터프리터가 설정되어있다.
'''
  


'''
배포 도구 오래된순
distutils(표준이자 기본 파이썬에내장 되어있음)
setuptools
easy_install
pip
선택은 자유인듯
'''

'''
리스트 내장 List Comprehensions 은 연산한 결과로 새로운 리스트를 만든다
[ 표현식 for 아이템 in 시퀸스타입 (if 조건식) ]
'''

def NewlistWithLeng():
  l = ["apple", "뭐냐?", "대한민국", "kit2013"]
  print ( [ i for i in l > len(i) >5 ] )
  
def 복수리스트():
  L1=[3,4,5]
  L2=[1.5,-0.5,4]
  print( [x*y for x in L1 for y in L2] )
  
'''
필터는 리스트르르 if문으로 필터링하는걸 간추린 역할을 한다
filter( function이 | None, <이터레이션 가능한 자료형>)
반환은 이터레이터를 반환한
'''

def FilterEaxmple():
  def Filter(i):
    return i > 20
  dummy = [1, 5, 25, 62, 27, 722]
  titer = filter(Filter , dummy)
  #titer = filter(lambda i: i > 20 , dummy)
  print( "반환은 이터레이터이지만 리스트로 만들어출력 \n{0}".format( list(titer) ) )
  return titer

'''
zip 함수는 3개 이상의 시퀸스형이나 이터레이터형 객체를 튜플형태로 쌍지을수있
'''

def ZipTest():
  a = ["sd", "test", "ㅁㄴㅇ"]
  #쌍이 맞지 않으면 작은걸 기준으로 맺어진
  b = "랒라수ㅁㄴㅇ"
  결과 = list(zip(a,b))
  #분리는 a, b = (zip*결과)로 분리시킬수있다
  return 결과

'''
순회가는한 객체의 모든 값을 갱신할땐 map함수를 써라
map(함수이름, 이터레이션 가능객체, ...)
결과값은 이터레이터로 반환한다 
'''

def MapTest():
  더미 = [2,5,6,16,36]
  return list(map((lambda i : i+100),더미))

'''
전부 순회할떈 문자열의 내장메서드인 join이나 리스트 내장을 사용해라
for문은 계속 printf를 출력하지만 join은 한번만 출력한
더 성능이 좋다 
'''

def JoinTest():
  raw = ["한글",2,"sawdㅁㅈㅇㅁㅈㅇ"]
  print(join(raw))


"""
클래스를 선언하면 별도의 이름공간이 생성된다
"""
# (object) 생략해도 자동적으로 적용된다
class militery:
  #멤버 변수
  comment = "군인"
  #멤버 메소드 모든 메소드는 첫번째 인자로 인스턴스의 레퍼런스가 전달된다
  def getComment(self):
    print("이것은 {0}".format(self.comment))

#인스턴스는 자체의 값이 변경되기 전까지 클래스를 레퍼런스한다(클래스와 공유)
def Test_ClassWithInstance():
  print(militery.comment)
  airforce = militery()
  print(airforce.comment)

  print("참조되는 클래스의 멤버변수를 변경")
  militery.comment = "군인이긴 한데 변경"
  print(airforce.comment)
  print("보다시피 클래스를 변경했는데 인스턴스도 변경된다")

'''
인스턴스.메소드() - 바운드 메소드
클래스.메소드(인스턴스) - 언바운드 메소드
멤버함수는 첫번재 인자로 객체의 레퍼런스를 받는걸 이용
self.멤버변수를 사용하니까 클래스에 인스턴스를 인자로 주어도
인스턴스의 데이터를 인스턴스의 메소드로 이용가능
name space 참조순서는 인스턴스 - 클래스 - 전역 이다.
'''

#동적으로 멤버변수를 추가하고 삭제할수있다.
def Test_dynamicmamber():
  #위의 객체 airforce의 생명주기가 끝난다. 그래서 새로생성
  airforce = militery()
  militery.rank = 2
  #인스턴스인 airforce도 rank를 갖는다
  print(airforce.rank)
  airforce.name = "공군"
  #인스턴스에 멤버추가하면 클래스에는 갱신없다 
  print(militery.name)

'''
인스턴스도 __class__.멤버변수 를통해 만들어진 클래스를 조작할수있고
이로 인해 생성되고 참조된 모든 인스턴스도 해당값으로 변경된다.
isinstance(인스턴스, 클래스) 를통해 어떤 클래스로부터 생성되었는지 판단할수있다)
'''

#생성자와 소멸자 테스트용
class 생성자와소멸자:
 __갯수 = 0
 def __init__(self, _str):
  self.value = _str
  생성자와소멸자.__갯수 += 1
  print("생성자호출 값은", self.value)

 def __del__(self):
  print("소멸자 호출")
#del 키워드로 직접 소멸시켜도 왜 소멸자 호출이안되는거?
 def member_func(self):
  print("인스턴스 갯수", self.__갯수)
 멤버함수 = classmethod(member_func)
  #정적메소드 등록할시에는 self가 필요없다
  #하지만 등록된 이름으로만 사용해야한다 ㅇㅇ
  #한마디로 언어가 static메소드를 제공하는게 아니라 개발자 임의로 구현하는거임 
 def static_func():
  print("인스턴스 갯수", 생성자와소멸자.__갯수)
 정적메소드 = staticmethod(static_func)
 
'''
오버로딩은 그냥 레퍼런스 보면서 하면되
'''

'''
파이썬 3.0 부터 모든 객체는 object를 상속한다
__dict__ 객체를 통해 클래스 정보를 볼수있다.
issubclass(자식, 부모) 메소드를 통해 판별 가능하다
__bases__ 속성을 통해 부모클래스를 튜플로 반환한다.
dir메소드로 메소드목록이나 객체 목록 출력 
'''

'''
def class a
 __init__(self, 멤버):
 self.멤버=멤버

def class b(a)
 __init__(self, 멤버, 추가된멤버)
 self.멤버 = 멤버
 self.추가된멤버 = 추가된멤
 또는 명시적으로 a.__init__(self,인자) 가능 이때 언바운드인게 주의
'''

'''
오버라이딩은 C++과 다르게 메소드 이름만 일치하면 된다
또는
def 오버라이딩할 메소도(self):
 부모클래스.오버라이딩할 메소드(self)
 자식클래스에서 추가할 로직
위방식대로도 가능하다 이를통해 자식클래스가 부모보다 이름공간이 검색규칙이 우선됨
'''
'''
다중상속은 그냥 (상속할클래스1, 상속할클래스2, ....) 하면됨
다중상속시 메소드 이름찾는 순서는 __mro__에 튜플로 정의되어있다
super.메소드 명등을 통해 부모클래스 의 메소드를 활용가능하며 인자로
상위 클래스를 명시할수있다.
'''


#모듈이다!!!! 















